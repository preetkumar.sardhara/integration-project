const express = require('express');
require('dotenv').config()
const slack = require('./api/slack')
const gitlab = require('./api/gitlab');

const app = express();

app.use('/slack', slack);
app.use('/gitlab',gitlab);

app.listen(3000);
console.log('You are listening to port 3000');