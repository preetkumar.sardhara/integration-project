const SLACK_ENTITY = {
    GROUP: 'group',
    USER: 'user',
    CHANNEL: 'channel'
}

module.exports = SLACK_ENTITY;