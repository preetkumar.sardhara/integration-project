const express = require("express");
const sendMessage = require("./send-message");
const router = express.Router();

router.use('/sendmessage', sendMessage);

module.exports = router;