const express = require("express");
const bodyParser = require("body-parser");
const sendMessage = require("./send-message")

const router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

router.post('/', sendMessage);

module.exports = router;