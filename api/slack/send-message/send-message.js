const SLACK_ENTITY = require("../slack-entity");
const slackBot = require("slackbots");

const botName = 'SlackBot';

const bot = new slackBot({
    token: process.env.SLACK_TOKEN,
    name: botName
});

const SLACK_ENTITY_TO_BOT_API = {
    [SLACK_ENTITY.USER]: 'postMessageToUser',
    [SLACK_ENTITY.GROUP]: 'postMessageToGroup',
    [SLACK_ENTITY.CHANNEL]: 'postMessageToChannel'
}

const SLACK_PARAMS = {
    type: 'mrkdwn'
};

module.exports = function(req,res) {
    const { type, name, message } = req.body;
    bot[SLACK_ENTITY_TO_BOT_API[type]](name,message,SLACK_PARAMS);
    res.status(200).end();
};