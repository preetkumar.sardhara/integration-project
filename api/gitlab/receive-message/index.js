const express = require("express");
const bodyParser = require("body-parser");
const receiveMessage = require("./receive-message");

const router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.post('/', receiveMessage);

module.exports = router;