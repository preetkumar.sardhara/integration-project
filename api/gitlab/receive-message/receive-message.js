const GITLAB_EVENTS_TO_MESSAGE_CREATOR_METHOD = require("./gitlab-events-to-message-creator-method");
const messageCreater = require("../../helpers/message-creator");
const request = require("request");

module.exports = async function(req,res) {
    const data = await messageCreater[GITLAB_EVENTS_TO_MESSAGE_CREATOR_METHOD[req.body.object_kind]](req.body);
    if(data.name === 'preetkumar.sardhara') {
        data.name = 'preetsardhara1999';
    }
    else{
        console.log(data);
    }
    request.post(process.env.LOCALHOST_URL).form(data);
    res.status(200).end();
};