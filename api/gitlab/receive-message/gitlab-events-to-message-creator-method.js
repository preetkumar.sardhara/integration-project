const MESSAGE_TYPES = require("../../helpers/message-creator/message-types");
const GITLAB_EVENTS = require("../gitlab-events");

const GITLAB_EVENTS_TO_MESSAGE_CREATOR_METHOD = {
    [GITLAB_EVENTS.NOTE]: MESSAGE_TYPES.COMMENT
}

module.exports = GITLAB_EVENTS_TO_MESSAGE_CREATOR_METHOD;