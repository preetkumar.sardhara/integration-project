const fetch = require("node-fetch");

module.exports = async function(id) {
    let data = await fetch(`https://prod-gitlab.sprinklr.com/api/v4/users/${id}?private_token=${process.env.PRIVATE_TOKEN}`).then(res => res.json());
    return data.username;
}