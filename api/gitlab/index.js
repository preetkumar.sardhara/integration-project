const express = require("express");
const receiveMessage = require("./receive-message");
const router = express.Router();

router.use('/receivemessage', receiveMessage);

module.exports = router;