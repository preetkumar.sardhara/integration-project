const MESSAGE_TYPES = require("./message-types");
const note= require("./comment-message-creator");

module.exports = {
    [MESSAGE_TYPES.COMMENT] : note
}