const findUserNameByID = require("../../gitlab/services/user-name-fetcher-by-id");
const SLACK_ENTITY = require("../../slack/slack-entity");

module.exports = async function(req) {
    const obj = {};
    switch(req.object_attributes.noteable_type) {
        case 'MergeRequest':
            obj.type = SLACK_ENTITY.USER;
            obj.name = await findUserNameByID(req.merge_request.author_id);
            obj.message = 'A comment was made in your merge request <' + req.object_attributes.url + '|!' + req.merge_request.id + '>\n>*' + req.user.name + '*: ```' + req.object_attributes.note + '```';
            break;
    }
    return obj;
}